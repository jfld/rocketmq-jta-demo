package com.xxx.points.dao;

import com.xxx.common.domain.TPoints;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by Sinotn
 *
 * @Author: libin
 * @CreateTime: 2020-10-28 11:35
 * @Description: 积分DAO
 */
@Mapper
public interface TPointsMapper {
    /**
     * 插入积分
     *
     * @param tPoints 积分实体
     * @return 结果
     */
    int insertTPoints(TPoints tPoints);

    /**
     * 根据订单查积分记录
     *
     * @param orderNo
     * @return 结果
     */
    TPoints selectTPointsById(Long orderNo);
}
