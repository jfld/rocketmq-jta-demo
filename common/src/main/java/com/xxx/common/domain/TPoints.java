package com.xxx.common.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Sinotn
 *
 * @Author: libin
 * @CreateTime: 2020-10-28 14:53
 * @Description: 积分实体
 */
@Data
public class TPoints implements Serializable {

    private Long id;

    private Long userId;

    private Long orderNo;

    private Integer points;

    private String remarks;
}
