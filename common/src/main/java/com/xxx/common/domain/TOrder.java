package com.xxx.common.domain;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by Sinotn
 *
 * @Author: libin
 * @CreateTime: 2020-10-28 11:12
 * @Description: 订单DAO
 */
@Data
public class TOrder implements Serializable {

    private Long orderId;

    private LocalDateTime createTime;

    private Long userId;

    private BigDecimal amount;
}
